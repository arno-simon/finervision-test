## Arno's Laravel Test Project for Finervision

This project uses the Laravel and React frameworks. 

## Setting up

```
git clone https://arno-simon@bitbucket.org/arno-simon/finervision-test.git <projectName>
cd <projectName>
composer install
mv .env.example .env
php artisan key:generate
npm install && npm run prod
```

## Database
Check the .env file to make sure the connection settings are matching the ones from your environment

Run the migrations:

```
php artisan migrate
```
## Server
You will find some help on how to configure your server here https://laravel.com/docs/5.6#installing-laravel

## Note
If you are using Homestead on a Windows machine, you should run **npm install && npm run prod** from Windows and not from the virtual machine.


### License

This project is open-source and licensed under the [MIT license](http://opensource.org/licenses/MIT)