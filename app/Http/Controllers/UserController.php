<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    public function getNewUser()
    {
    	return view('welcome');
    }

    public function postNewUser(Requests\CreateUserRequest $request)
    {
    	//Create user
    	$user = new User();

    	//Fill attributes
        $user->firstname = $request->get('firstname');
        $user->surname = $request->get('surname');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->gender = $request->get('gender');
        $user->comments = $request->get('comments');


        // Format date
        $date = Carbon::createFromDate($request->get('year'), $request->get('month'), $request->get('day'));
        $user->date_of_birth = $date;

        // Save user
        $user->save();
        
        return redirect()->back()->with(['status' => $user->firstname.' '.$user->surname.' has been successfully added']);
    }
}
