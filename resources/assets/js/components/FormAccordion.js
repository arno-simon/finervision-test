import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import FormValidator from './FormValidator';


class FormAccordion extends Component {

    constructor(props) {
        super(props);

        // Step 1 validator containing validation rules
        this.validator1 = new FormValidator([
            { 
                field: 'firstname', 
                method: 'isEmpty', 
                validWhen: false, 
                message: 'First Name is required.' 
            },
            { 
                field: 'surname', 
                method: 'isEmpty', 
                validWhen: false, 
                message: 'Surname is required.' 
            },
            { 
                field: 'email', 
                method: 'isEmpty', 
                validWhen: false, 
                message: 'Email is required.' 
            },
            { 
                field: 'email',
                method: 'matches', 
                args: [/^[^@\s]+@[^@\s]+\.[^@\s]+$/],
                validWhen: true, 
                message: 'Please provide an valid email address.'
            },
        ]);

        // Step 2 validator containing validation rules
        this.validator2 = new FormValidator([
            { 
                field: 'phone', 
                method: 'isEmpty', 
                validWhen: false, 
                message: 'Phone number is required.'
            },
            {
                field: 'phone', 
                method: 'matches',
                args: [/^07(\d ?){9}$/], // args is an optional array of arguements that will be passed to the validation method
                validWhen: true, 
                message: 'Invalid phone number. Please enter a number as 07XXXXXXXXX'
            },
            {
                field: 'gender', 
                method: 'isEmpty',
                validWhen: false, 
                message: 'Gender is required.'
            },
            {
                field: 'gender', 
                method: 'matches',
                args: [/^M|F$/], // args is an optional array of arguements that will be passed to the validation method
                validWhen: true, 
                message: 'That is not a valid gender.'
            },
            {
                field: 'day', 
                method: 'isEmpty',
                validWhen: false, 
                message: 'Day is required.'
            },
            {
                field: 'day', 
                method: 'isInt',
                args: [{min: 1, max: 31}], // args is an optional array of arguements that will be passed to the validation method
                validWhen: true, 
                message: 'The day must be between 1 and 31.'
            },
            {
                field: 'month', 
                method: 'isEmpty',
                validWhen: false, 
                message: 'Month is required.'
            },
            {
                field: 'month', 
                method: 'isInt',
                args: [{min: 1, max: 12}], // args is an optional array of arguements that will be passed to the validation method
                validWhen: true, 
                message: 'The month must be between 1 and 12.'
            },
            {
                field: 'year', 
                method: 'isEmpty',
                validWhen: false, 
                message: 'Year is required.'
            },
            {
                field: 'year', 
                method: 'isInt',
                args: [{min: 1900, max: new Date().getFullYear()}], // args is an optional array of arguements that will be passed to the validation method
                validWhen: true, 
                message: 'The year must be between 1900 and '+ new Date().getFullYear()+'.'
            },
        ]);


        this.state = {
            firstname: '',
            surname: '',
            email: '',
            phone: '',
            gender: '',
            day: '',
            month: '',
            year: '',
            comments: '',
            validation1: this.validator1.valid(),
            validation2: this.validator2.valid(),

        };

        this.submitted = false;

        this.handleChange = this.handleChange.bind(this);
        this.handleStep1 = this.handleStep1.bind(this);
        this.handleStep2 = this.handleStep2.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateStep1 = this.validateStep1.bind(this);
        this.validateStep2 = this.validateStep2.bind(this);
    }

    // Update state when user input changes
    handleChange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
    }

    // Validate step 1 inputs and move to step 2 if inputs are valid
    handleStep1(event) {
        event.preventDefault();

        const validation1 = this.validateStep1();

        if (validation1.isValid) {
            $('#collapseTwo').collapse('show');
        }else{
            $('#collapseOne').collapse('show');
        }

        return validation1.isValid;
    }

    // Validate step 2 inputs and move to step 3 if inputs are valid
    handleStep2(event) {
        event.preventDefault();

        const validation2 = this.validateStep2();

        if (validation2.isValid) {
            $('#collapseThree').collapse('show');
        }else{
            $('#collapseTwo').collapse('show');
        }

        return validation2.isValid;
    }

    // Validate step 1 and step 2 inputs and submit form
    handleSubmit(event) {

        event.preventDefault();

        const validation1 = this.validateStep1();
        const validation2 = this.validateStep2();

        if(validation1.isValid && validation2.isValid)
        {
            $('#form').submit();
        }else if(!validation1.isValid){
            $('#collapseOne').collapse('show');
        }else{
            $('#collapseTwo').collapse('show');
        }

        this.submitted = true;
    }

    // Validate step 1 inputs
    validateStep1(){

        const validation1 = this.validator1.validate(this.state);
        this.setState({ validation1 });

        return validation1;
    }

    // Validate step 2 inputs
    validateStep2(){

        const validation2 = this.validator2.validate(this.state);
        this.setState({ validation2 });

        return validation2;
    }

    render() {
        let validation1 = this.submitted ?                         // if the form has been submitted at least once
                      this.validator1.validate(this.state) :   // then check validity every time we render
                      this.state.validation1                   // otherwise just use what's in state

        let validation2 = this.submitted ?                         
                    this.validator2.validate(this.state) :   
                    this.state.validation2                   

        return (
            <form onSubmit={this.handleSubmit} method="POST"
                          action="/" encType="multipart/form-data" id="form">
                <input type="hidden" name="_token" value={this.props.token}/>
                <div className="accordion" id="accordionExample">
                  <div className="card">
                    <div className="card-header" id={this.state.firstname}>
                      <h5 className="mb-0">
                        <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne" onClick={this.handleCollapse}>
                          Step 1: Your details
                        </button>
                      </h5>
                    </div>

                    <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div className="card-body">
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-4">
                                    <label htmlFor="firstname">First Name</label>
                                    <span className="help-block">{validation1.firstname.message}</span>
                                    <input type="text" className={validation1.firstname.isInvalid ? 'form-control has-error' : 'form-control'} id="firstname" name="firstname" value={this.state.firstname} onChange={this.handleChange}/>
                                </div>
                                    
                                <div className="col-md-4">
                                    <label htmlFor="surname">Surname</label>
                                    <span className="help-block">{validation1.surname.message}</span>
                                    <input type="text" className={validation1.surname.isInvalid ? 'form-control has-error' : 'form-control'} id="surname" name="surname" value={this.state.surname} onChange={this.handleChange}/>
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-4">
                                    <label htmlFor="email">Email Address</label>
                                    <span className="help-block">{validation1.email.message}</span>
                                    <input type="email" className={validation1.email.isInvalid ? 'form-control has-error' : 'form-control'} id="email" name="email" value={this.state.email} onChange={this.handleChange}/>
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <div className="row">
                                <div className="col offset-md-8 text-right">
                                    <button type="button" className="btn next" onClick={this.handleStep1}>Next ></button>
                                </div>
                            </div>
                        </div>

                      </div>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-header" id="headingTwo">
                      <h5 className="mb-0">
                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Step 2: More comments
                        </button>
                      </h5>
                    </div>
                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div className="card-body">
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-4">
                                    <label htmlFor="phone">Telephone number</label>
                                    <span className="help-block">{validation2.phone.message}</span>
                                    <input type="text" className={validation2.phone.isInvalid ? 'form-control has-error' : 'form-control'} id="phone" name="phone" value={this.state.phone} onChange={this.handleChange}/>
                                </div>
                                    
                                <div className="col-md-3">
                                    <label htmlFor="gender">Gender</label>
                                    <span className="help-block">{validation2.gender.message}</span>
                                        <select name="gender" className={validation2.gender.isInvalid ? 'form-control has-error' : 'form-control'} id="gender" value={this.state.gender} onChange={this.handleChange}>
                                          <option value="" disabled>Select Gender</option>
                                          <option value="M">M</option>
                                          <option value="F">F</option>
                                        </select>
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-8 col-lg-4">
                                    <label htmlFor="day">Date of birth</label>
                                    <span className="help-block d-block">{validation2.day.isInvalid ? validation2.day.message : ''}</span>
                                    <span className="help-block d-block">{validation2.month.isInvalid ? validation2.month.message : ''}</span>
                                    <span className="help-block d-block">{validation2.year.isInvalid ?  validation2.year.message : ''}</span>

                                    <div className="form-row" id="dob">
                                        <div className="form-group col-xs-4 col-md-4 col-lg-3">
                                            <input type="number" className={validation2.day.isInvalid ? 'form-control has-error' : 'form-control'} id="day" min="1" max="31" name="day" value={this.state.day} onChange={this.handleChange}/>
                                        </div>
                                        <div className="form-group col-xs-4 col-md-4 col-lg-3">
                                            <input type="number" className={validation2.month.isInvalid ? 'form-control has-error' : 'form-control'} id="month" min="1" max="12" name="month" value={this.state.month} onChange={this.handleChange}/>
                                        </div>
                                        <div className="form-group col-xs-4 col-md-4 col-lg-3">
                                            <input type="number" className={validation2.year.isInvalid ? 'form-control has-error' : 'form-control'} id="year" minLength="4" maxLength="4" name="year" value={this.state.year} onChange={this.handleChange}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div className="form-group">
                            <div className="row">
                                <div className="col offset-md-8 text-right">
                                    <button type="button" className="btn next" onClick={this.handleStep2}>Next ></button>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingThree">
                      <h5 className="mb-0">
                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Step 3: Final comments
                        </button>
                      </h5>
                    </div>
                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                      <div className="card-body">
                        <div className="form-group">
                            <div className="row">
                                <div className="col">
                                    <label htmlFor="comments">Comments</label>
                                    <textarea className="form-control" rows="3" id="comments" name="comments" value={this.state.comments} onChange={this.handleChange}></textarea>
                                </div>
                                    
                                <div className="col text-right position-relative">
                                    <button type="submit" className="btn next">Next ></button>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </form>
        );
    }
}

if (document.getElementById('app')) {
    const csrfToken = document.head.querySelector("[name=csrf-token]").content;
    ReactDOM.render(<FormAccordion token={csrfToken}/>, document.getElementById('app'));
}

