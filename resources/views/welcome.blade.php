<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Finervision - Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">

    </head>
    <body> 
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session()->has('status'))
                <div class="alert alert-success">
                    <div class="bg-green alert-icon">
                        <i class="glyph-icon icon-check"></i>
                    </div>
                    <div class="alert-content">
                        <h4 class="alert-title">{{ session('status') }}</h4>
                    </div>
                </div>
            @endif

            <div id="app"></div>
        </div> 
        
        <!-- JS -->
        <script type="text/javascript" src="{{ url('js/app.js') }}"></script>
    </body>
</html>
